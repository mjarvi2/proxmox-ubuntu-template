#!/bin/bash

createfile () {
        tmpfile=$1
        echo "#!/bin/bash" > $tmpfile
}


writeecho () {
        tmpfile=$1
        key=$2
        value=$3
        echo "echo ${key}=${value}" >> $tmpfile

}

taghandler () {
        tags=$1
        tmpfile=$2
        IFS='>' read -r -a array <<< "$tags"
        for element in "${array[@]}"
        do
                IFS='.' read -r key value <<< $element
                writeecho $tmpfile $key $value
        done
}

finish () {
        tmpfile=$1
        filename=$2
        dir="/etc/facter/facts.d/"
        mkdir -p $dir
        mv $tmpfile $dir
        chmod u+x $dir$filename
}

filename="external-factstest.sh"
tmpfile="/tmp/${filename}"
createfile $tmpfile
taghandler $1 $tmpfile
finish $tmpfile $filename
