#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
#because ubuntu would rather have networking not work than you not being tracked
echo "search jarvis.boss" > /etc/resolv.conf
echo "nameserver 192.168.51.133" >> /etc/resolv.conf

if command -v yum >/dev/null; then
	echo "Detected a yum based box"
	break=0
	while [[ break -eq 0 ]]; do
                echo "waiting for yum lock"
		sleep 5;
                if ! pgrep -x "dnf|yum" > /dev/null; then
                        break=1
                        echo "no lock found, proceeding..."
                fi
        done
	echo "Installing puppet"
        rpm -Uvh https://yum.puppet.com/puppet7-release-el-8.noarch.rpm
        yum install puppet-agent -y
	echo "disable SE"
	setenforce 0

elif command -v apt-get >/dev/null; then
	echo "detected an apt based box"
        while sudo fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do
                sleep 1
        done
	echo "Installing puppet"
        apt install wget -y
        wget -O /tmp/puppet.deb https://apt.puppetlabs.com/puppet7-release-focal.deb
        dpkg -i /tmp/puppet.deb
        apt update
        apt install puppet-agent -y
else
        exit 1
fi

dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
mv $dir/ssl /etc/puppetlabs/puppet
mv $dir/puppet.conf /etc/puppetlabs/puppet/puppet.conf
echo "Running facter script before first puppet run"
$dir/facter.sh $1
/opt/puppetlabs/puppet/bin/puppet agent -t
systemctl enable --now puppet

