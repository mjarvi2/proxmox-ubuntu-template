#!/bin/bash

get_current_vmids () {
	# grabs the current vm ids so we dont cause a collision
	local vm_metadata="/etc/pve/.vmlist"
	local vmjson=$(jq -r '.ids | keys' $vm_metadata)
	local count=$(jq length <<< "$vmjson")
	local vmids=()
	for (( i=0; i < count; i++ ))
	do
    		local vmid=$(jq -r ".[$i]" <<< "$vmjson")
    		vmids=( ${vmids[@]} $vmid )
	done
	printf '%s\n' "${vmids[@]}"
}

pick_vmid () {
	# picks the vmid that will be used for the template
	local target_id=$1
	local end_id=$2
	local current_ids="$(get_current_vmids)"
	for id in $current_ids; do
		if [ $target_id -eq $id ]; then
			if [ $target_id -eq $end_id ]; then
				printf "\nNo VM IDs available. exiting...\n"
				exit 1
			fi
			((target_id++))
		fi
	done
	echo $target_id
}

get_qcow () {
	# fetches and renames the cloud image
	local image_url=$1
	local qcow_dir=$2
	local qcow_file=$3
	local name=$4
	mkdir -p $qcow_dir
	wget -O /tmp/$name.img $image_url
	mv /tmp/$name.img $qcow_file
	qemu-img resize $qcow_file 32G
}

disk_config () {
	# configures the disk needed for the template
	local vmid=$1
	local qcow_location=$2
	qm importdisk $vmid $qcow_location local-lvm
	qm set $vmid -scsihw virtio-scsi-pci -virtio0 local-lvm:vm-$vmid-disk-0
      	qm set $vmid -serial0 socket
	qm set $vmid -boot c -bootdisk virtio0	
}

custom_configurator () {
        local vmid=$1
        local user=$2
        local password=$3
	local ip_to_use=$4
	local ssh_keys=$5
	local name=$6
	local nameserver=$7
	local priv_ssh_key="$dir/ssh_key"
	local pub_ssh_key="$priv_ssh_key.pub"
	# create temporary ssh key for us to use
	printf "running userdata configurator\n"
	dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
	ssh-keygen -t rsa -C "proxmox@proxmox" -f $priv_ssh_key -P "" &> /dev/null
        qm set $vmid -agent 1
        qm set $vmid -ide2 local-lvm:cloudinit
        qm set $vmid --sshkey $pub_ssh_key
        qm set $vmid --ipconfig0 ip=$ip_to_use
        qm set $vmid --cipassword=$password --ciuser=$user
	qm set $vmid -cores 4 -memory 4096
	#for testing
	raw_ip=$(sed 's:/[^/]*$::' <<< $ip_to_use)
	cat /dev/null > ~/.ssh/known_hosts
	qm start $vmid
	printf "Giving time for cloudinit to happen\n"
	sleep 180
	ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo mkdir /bootstrap"
	if [[ $name == *"alma"* ]]; then
		ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo sed -i 's/enforcing/disabled/g' /etc/selinux/config"
		ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo bash -c \"echo /dev/null > /etc/sysconfig/qemu-ga\""
	else
		# setting dns for ubuntu
		ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo bash -c \'echo nameserver $nameserver > /etc/resolv.conf\'"
		ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo apt update"
		ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo apt install qemu-guest-agent -y"
		ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo systemctl enable qemu-guest-agent"
	fi
	scp -o "StrictHostKeyChecking=no" -i $priv_ssh_key -r "$dir/userdata-confs" $user@$raw_ip:/home/$user
	ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo mv /home/$user/userdata-confs/* /bootstrap"
	ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo chmod +x /bootstrap/*"
	ssh -o "StrictHostKeyChecking=no" -i $priv_ssh_key $user@$raw_ip "sudo shutdown now"
	sleep 10 # bad things can happen if we dont give proxmox a sec to realize the box is shut down
	#qm set $vmid --nameserver $nameserver
	rm -f $priv_ssh_key $pub_ssh_key
	qm set $vmid --ipconfig0 ip=dhcp
	qm set $vmid --sshkey $ssh_keys
}

misc_config () {
	qm set $vmid -hotplug disk,network,usb
	qm set $vmid -vga qxl
	qm set $vmid --onboot 1
}

vm_creation_pipeline () {
	# handles the creation and modifications to the template
	local vmid=$1
	local name=$2
	local image_url=$3
	local ssh_keys=$4
	local qcow_dir=$5
	local user=$6
	local password=$7
	local ip_to_use=$8
	local nameserver='192.168.51.133'
	printf "\nPIPELINE PARAMS\n$vmid\n$name\n$image_url\n$ssh_keys\n$qcow_dir\n\n"
	local qcow_file="$qcow_dir/$qcow_file.qcow2"
	printf "qcow file $qcow_file\n"
	echo "generating qcow file"
	get_qcow $image_url $qcow_dir $qcow_file $name
	create_vm $vmid $name
	disk_config $vmid $qcow_file
	custom_configurator $vmid $user $password $ip_to_use $ssh_keys $name $nameserver
	misc_config
	qm template $vmid
}

create_vm () {
	local vmid=$1
	local name=$2
	qm create $vmid -name $name -memory 1024 -net0 virtio,bridge=vmbr0 -cores 1 -sockets 1
}


template_resource_pool () {
	local vmid=$1
	local resource_pool=$2
	pvesh create /pools --poolid $resource_pool
		
}

runner () {
	#params
	local resource_pool_name=$(echo $1 | xargs)
	local name=$(echo $2 | xargs)
	local vmid_start=$(echo $3 | xargs)
	local vmid_end=$(echo $4 | xargs)
	local qcow_dir=$(echo $5 | xargs)
	local ssh_keys=$(echo $6 | xargs)
	local image_location=$(echo $7 | xargs)
	local user=$(echo $8 | xargs)
	local password=$(echo $9 | xargs)
	local ip_to_use=$(echo "${10}" | xargs)
	printf "$1\n$2\n$3\n$4\n$5\n$6\n"
	local vmid="$(pick_vmid $vmid_start $vmid_end)"
	vm_creation_pipeline $vmid $name $image_location $ssh_keys $qcow_dir $user $password $ip_to_use
	template_resource_pool $vmid $resource_pool_name
	rm -f $ssh_keys
}

main () {
	printf "Creating proxmox templates\nHAJIME!\n\n"
	local dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
	local config_file="config.json"
	local template_start_id=$(jq -r '.template_start_id' $config_file)
	local template_end_id=$(jq -r '.template_end_id' $config_file)
	local resource_pool=$(jq -r '.resource_pool' $config_file)
	local qcow_dir=$(jq -r '.qcow_dir' $config_file)
	local ip_to_use=$(jq -r '.temporary_ip' $config_file)
	printf "qcow dir from config file $qcow_dir\n"
	local templates=( $(cat "$config_file" | jq -r '.templates | keys[]') )
	for conf in "${templates[@]}"; do
		local name=$conf
		local url=$(jq -r '.templates."'"$name"'".img_url' $config_file)
		local user=$(jq -r '.templates."'"$name"'".user' $config_file)
		local password=$(jq -r '.templates."'"$name"'".password' $config_file)
		local temp_keys_file=$dir/$name-keys.pub
		touch $temp_keys_file
		jq -c -r '.templates."'"$name"'".ssh_keys[]' $config_file | while read i; do
			echo $i >> $temp_keys_file
		done
		runner $resource_pool $name $template_start_id $template_end_id	$qcow_dir $temp_keys_file $url $user $password $ip_to_use
	done
	printf "\nDUNZO!\n\n"
}
main
