# proxmox ubuntu template creator

## usage

### clone the repo

`git clone https://gitlab.com/mjarvi2/proxmox-ubuntu-template.git` to your proxmox instance

### configure the config.json file

Set the default usernames, passwords, ssh keys, and network for your desired templates

### Set the user data script

Edit the userdata.sh to install anything that is needed by your provisioning software

### Run the template creator

`chmod +x templator.sh`
`./templator.sh`

NOTE: Some instances may get stuck in the proxmox console and will require you to hit enter while the script is running

